﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZechFinalProg
{
    class Stay
    {
        //-roomList:List
        //-checkInDate: DateTime
        //-checkOutDate: DateTime

        private List<HotelRoom> roomList = new List<HotelRoom>();
        public List<HotelRoom> RoomList
        {
            get { return roomList; }
            set { roomList = value; }
        }

        private DateTime checkInDate;
        public DateTime CheckInDate
        {
            get { return checkInDate; }
            set { checkInDate = value; }
        }

        private DateTime checkOutDate;
        public DateTime CheckOutDate
        {
            get { return checkOutDate; }
            set { checkOutDate = value; }
        }

        //+Stay()
        //+Stay(DateTime,DateTime)
        //+AddRoom(HotelRoom)
        //+CalculateTotal():double
        //+ToString: string

        public Stay() { }
        public Stay(DateTime inCheck, DateTime outCheck)
        {
            checkInDate = inCheck;
            checkOutDate = outCheck;
        }

        public void AddRoom(HotelRoom h)
        {
            roomList.Add(h);
        }

        public double CalculateTotal()
        {
            double total = 0;
            double diff = (checkInDate - checkOutDate).TotalDays;
            foreach (HotelRoom r in roomList)
            {
                total += (diff * r.DailyRate);
            }

            return total;
        }

        public override string ToString()
        {
            return $"CheckIN: {checkInDate} CheckOUT: {checkOutDate}";
        }

    }
}
