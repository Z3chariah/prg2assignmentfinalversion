﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZechFinalProg
{
    class Guest
    {

        //-name: string
        //-ppNumber: string
        //-hotelStay: Stay
        //-membership: Membership
        //isCheckedIn: bool

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string ppNumber;
        public string PpNumber
        {
            get { return ppNumber; }
            set { ppNumber = value; }
        }

        private Stay hotelStay;
        public Stay HotelStay
        {
            get { return hotelStay; }
            set { hotelStay = value; }
        }

        private Membership membership;
        public Membership Membership
        {
            get { return membership; }
            set { membership = value; }
        }

        private bool isCheckedIn;
        public bool IsCheckedIn
        {
            get { return isCheckedIn; }
            set { isCheckedIn = value; }
        }

        //+Guest()
        //+Guest(string,string,Stay,Membership,bool)
        //+ToString():string
        public Guest() { }
        public Guest(string n, string pp, Stay h, Membership m, bool tf)
        {
            name = n;
            ppNumber = pp;
            hotelStay = h;
            membership = m;
            isCheckedIn = tf;
        }

        public override string ToString()
        {
            return $"Name: {name} Passport Number: {ppNumber} Room: {hotelStay} Membership: {membership} Checked in: {isCheckedIn}";
        }
    }
}
