﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZechFinalProg
{
    class StandardRoom : HotelRoom
    {
        //-requireWifi: bool
        //requireBreakfast: bool
        private bool requireWifi;
        public bool RequireWifi
        {
            get { return requireWifi; }
            set { requireWifi = value; }
        }

        private bool requireBreakfast;
        public bool RequireBreakfast
        {
            get { return requireBreakfast; }
            set{ requireBreakfast = value; }
        }

        //+StandardRoom()
        //StandardRoom(string string string double bool int)
        //CalculateCharges(): double
        //ToString(): string
        public StandardRoom() : base() { }
        public StandardRoom( string rn, string b, double d, bool i, int n) : base("Standard", rn, b, d, i, n)
        {

        }
        public override double CalculateChanges(int daydiff)
        {
            if (requireWifi == true)
            {
                return DailyRate * daydiff + 10 * DailyRate;
            }
            else if (requireBreakfast == true)
            {
                return DailyRate * daydiff + 20 * DailyRate;
            }
            else if (requireBreakfast == true && requireWifi == true)
            {
                return DailyRate * daydiff + 30 * DailyRate;
            }
            else
            {
                return DailyRate * daydiff;
            }

        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
