﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ZechFinalProg
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<HotelRoom> roomlist = new List<HotelRoom>();
        List<HotelRoom> selectroomlist = new List<HotelRoom>();
        List<Guest> guestlist = new List<Guest>();
        public void InitData()
        {
            HotelRoom room1 = new StandardRoom("101", "Single", 90.0, false, 1);
            HotelRoom room2 = new StandardRoom("102", "Single", 90.0, true, 0);
            HotelRoom room3 = new StandardRoom("201", "Twin", 110.0, true, 0);
            HotelRoom room4 = new StandardRoom("202", "Twin", 110.0, false, 2);
            HotelRoom room5 = new StandardRoom("203", "Twin", 110.0, true, 0);
            HotelRoom room6 = new StandardRoom("301", "Triple", 120.0, true, 0);
            HotelRoom room7 = new StandardRoom("302", "Triple", 120.0, false, 3);
            HotelRoom room8 = new DeluxeRoom("204", "Twin", 140.0, true, 0);
            HotelRoom room9 = new DeluxeRoom("205", "Twin", 140.0, true, 0);
            HotelRoom room10 = new DeluxeRoom( "303", "Triple", 210.0, false, 4);
            HotelRoom room11 = new DeluxeRoom( "304", "Triple", 210.0, true, 0);

            roomlist.Add(room1);
            roomlist.Add(room2);
            roomlist.Add(room3);
            roomlist.Add(room4);
            roomlist.Add(room5);
            roomlist.Add(room6);
            roomlist.Add(room7);
            roomlist.Add(room8);
            roomlist.Add(room9);
            roomlist.Add(room10);
            roomlist.Add(room11);

            Membership m1 = new Membership("Gold", 280);
            Membership m2 = new Membership("Ordinary", 0);
            Membership m3 = new Membership("Silver", 190);
            Membership m4 = new Membership("Gold", 10);


            Stay stay1 = new Stay(new DateTime(2019 - 1 - 26), new DateTime(2019 - 2 - 2));
            Stay stay2 = new Stay(new DateTime(2019 - 1 - 25), new DateTime(2019 - 1 - 31));
            Stay stay3 = new Stay(new DateTime(2019 - 2 - 1), new DateTime(2019 - 2 - 6));
            Stay stay4 = new Stay(new DateTime(2019 - 1 - 28), new DateTime(2019 - 2 - 10));

            Guest guest1 = new Guest("Amelia", "S1234567A", stay1, m1, true);
            Guest guest2 = new Guest("Bob", "G1234567A", stay2, m2, true);
            Guest guest3 = new Guest("Cody", "G2345678A", stay3, m3, true);
            Guest guest4 = new Guest("Edda", "S3456789A", stay4, m4, true);
            guestlist.Add(guest1);
            guestlist.Add(guest2);
            guestlist.Add(guest3);
            guestlist.Add(guest4);
        }

        public MainPage()
        {
            this.InitializeComponent();
            InitData();
        }

        //Button unchanged @JunJie
        private void checkRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            List<HotelRoom> room = new List<HotelRoom>();
            foreach (HotelRoom r in roomlist)
            {
                if (r.IsAvail == true)
                {
                    room.Add(r);
                }
            }
            LvAvalibleRoom.ItemsSource = room;
        }

        private void addRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            HotelRoom sr = (HotelRoom)LvAvalibleRoom.SelectedItem;
            if (sr != null)
            {
                //Initializing a new Guest object
                DateTime CheckIn = DateTime.Parse(checkInDate.Date.ToString());
                DateTime CheckOut = DateTime.Parse(checkOutDate.Date.ToString());
                Membership m = new Membership("Ordinary", 0);
                Stay s = new Stay(CheckIn, CheckOut);
                Guest newGuest = new Guest(guestName.Text, passportNo.Text, s, m, false);
                Stay assn = newGuest.HotelStay;
                
                if (sr.RoomType == "Standard")
                {
                    roomSelect.Text = "Room Select: Standard";
                    //Bed config pricing
                    if (sr.BedConfiguration.ToUpper() == "SINGLE")
                    {
                        sr.DailyRate += 90;
                    }
                    else if (sr.BedConfiguration.ToUpper() == "DOUBLE")
                    {
                        sr.DailyRate += 110;
                    }
                    else if (sr.BedConfiguration.ToUpper() == "TRIPLE")
                    {
                        sr.DailyRate += 120;
                    }

                    if (addWifi.IsChecked == true)
                    {
                        sr.DailyRate += 10;
                    }
                    if (addBreakfast.IsChecked == true)
                    {
                        sr.DailyRate += 20;
                    }
                    assn.AddRoom(sr);
                    selectroomlist.Add(sr);
                }

                else if (sr.RoomType == "Deluxe")
                {
                    roomSelect.Text = "Room Select: Dexluxe";
                    //Bed config
                    if (sr.BedConfiguration.ToUpper() == "DOUBLE")
                    {
                        sr.DailyRate += 140;
                    }
                    if (sr.BedConfiguration.ToUpper() == "TRIPLE")
                    {
                        sr.DailyRate += 210;
                    }

                    if (addBed.IsChecked == true)
                    {
                        sr.DailyRate += 25;
                    }
                    assn.AddRoom(sr);
                    selectroomlist.Add(sr);
                }
                lvRoomSelect.ItemsSource = null;
                lvRoomSelect.ItemsSource = assn.RoomList;
            }
            else
            {
                roomSelect.Text = "Please select a room!";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = guestName.Text;
            HotelRoom r = (HotelRoom)lvRoomSelect.SelectedItem;
            selectroomlist.Remove(r);

            foreach (Guest g in guestlist)
            {
                if (name.ToUpper() == g.Name.ToUpper())
                {
                    Stay rm = g.HotelStay;
                    rm.RoomList.Remove(r);
                }
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void LvAvalibleRoom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ExtentStayBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void noChildren_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void noAdult_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
