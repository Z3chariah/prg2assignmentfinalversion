﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZechFinalProg
{
    abstract class HotelRoom
    {
        //-roomType:string
        //-roomNumber:string
        //-bedConfiguration: strinf
        //-dailyRate:double
        //isAvail:bool
        //-noOfOcupants: int
        private string roomType;
        public string RoomType
        {
            get { return roomType; }
            set { roomType = value; }
        }

        private string roomNumber;
        public string RoomNumber
        {
            get { return roomNumber; }
            set { roomNumber = value; }
        }

        private string bedConfiguration;
        public string BedConfiguration
        {
            get { return bedConfiguration; }
            set { bedConfiguration = value; }
        }

        private double dailyRate;
        public double DailyRate
        {
            get { return dailyRate; }
            set { dailyRate = value; }
        }

        private bool isAvail;
        public bool IsAvail
        {
            get { return isAvail; }
            set { isAvail = value; }
        }

        private int noOfOccupants;
        public int NoOfOccupants
        {
            get { return noOfOccupants; }
            set { noOfOccupants = value; }
        }

        //+HotelRoom()
        //_HotelRoom(string string string double bool int)
        //abstract CalculateCharges(): double
        //+ToString(): string
        public HotelRoom() { }
        public HotelRoom(string rt, string rn, string b, double d, bool i, int n)
        {
            roomType = rt;
            roomNumber = rn;
            bedConfiguration = b;
            dailyRate = d;
            isAvail = i;
            noOfOccupants = n;
        }

        public virtual double CalculateChanges(int daydiff)
        {
            return dailyRate * daydiff;
        }
        public override string ToString()
        {
            return $"Type: {roomType}, Number: {roomNumber}, Bed: {bedConfiguration}, Daily Rate: ${dailyRate}\n ";
        }
    }
}
