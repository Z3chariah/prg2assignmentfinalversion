﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZechFinalProg
{
    class DeluxeRoom : HotelRoom
    {
        //-additionalBed:bool
        private bool additionalBed;
        public bool AdditionalBed
        {
            get { return additionalBed; }
            set { additionalBed = value; }
        }

        //+DeluxeRoom()
        //+DeluxeRoom(string string string double bool int)
        //CalculateCharges
        //ToString(): string
        public DeluxeRoom() : base() { }
        public DeluxeRoom( string rn, string b, double d, bool i, int n) : base("Deluxe", rn, b, d, i, n)
        {

        }
        public override double CalculateChanges(int daydiff)
        {
            if (additionalBed == true)
            {
                return base.CalculateChanges(daydiff) + 25 * DailyRate;
            }
            else
            {
                return base.CalculateChanges(daydiff);
            }

        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
