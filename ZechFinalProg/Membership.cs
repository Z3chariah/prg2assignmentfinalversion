﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZechFinalProg
{
    class Membership
    {
        //-status:string
        //-points: int

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private int points;
        public int Points
        {
            get { return points; }
            set { points = value; }
        }

        //+Membership()
        //+Membership(string, int)
        //+EarnPoints(double)
        //+RedeemPoints(int):bool
        //+ToString():string
        public Membership() { }
        public Membership(string s, int p)
        {
            status = s;
            points = p;
        }

        public void EarnPoints(double d)
        {
            int dd = Convert.ToInt32(d);
            points += dd;
        }

        public bool RedeemPoints(int rd)
        {
            bool check = false;
            if (rd < points)
            {
                points -= rd;
                check = true;
            }

            return check;
        }

        public override string ToString()
        {
            return $"Status: {status} Points: {points}";
        }
    }
}
